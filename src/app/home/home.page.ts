import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ChartDataSets, ChartOptions} from 'chart.js';
import {Color, Label} from 'ng2-charts';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

    chartDataLine: ChartDataSets[] = [{data: [], label: 'Venta'}];
    chartLabelsLine: Label[];

    // Options
    chartOptionsLine = {
        responsive: true,
        title: {
            display: true,
            text: 'Historico de Ventas'
        },
        pan: {
            enabled: true,
            mode: 'xy'
        },
        zoom: {
            enabled: true,
            mode: 'xy'
        }
    };
    showLegend = false;

    history = [{
        'date': '2019-01-14',
        'open': 150.85,
        'high': 151.27,
        'low': 149.22,
        'close': 150.0,
        'volume': 3.24392E7,
        'unadjustedVolume': 3.24392E7,
        'change': 0.85,
        'changePercent': 0.563,
        'vwap': 150.16333,
        'label': 'January 14, 19',
        'changeOverTime': 0.00563
    }, {
        'date': '2019-01-15',
        'open': 150.27,
        'high': 153.39,
        'low': 150.05,
        'close': 153.07,
        'volume': 2.87109E7,
        'unadjustedVolume': 2.87109E7,
        'change': -2.8,
        'changePercent': -1.863,
        'vwap': 152.17,
        'label': 'January 15, 19',
        'changeOverTime': -0.01863
    }, {
        'date': '2019-01-16',
        'open': 153.08,
        'high': 155.88,
        'low': 153.0,
        'close': 154.94,
        'volume': 3.05697E7,
        'unadjustedVolume': 3.05697E7,
        'change': -1.86,
        'changePercent': -1.215,
        'vwap': 154.60667,
        'label': 'January 16, 19',
        'changeOverTime': -0.01215
    }, {
        'date': '2019-01-17',
        'open': 154.2,
        'high': 157.66,
        'low': 153.26,
        'close': 155.86,
        'volume': 2.98212E7,
        'unadjustedVolume': 2.98212E7,
        'change': -1.66,
        'changePercent': -1.077,
        'vwap': 155.59333,
        'label': 'January 17, 19',
        'changeOverTime': -0.01077
    }, {
        'date': '2019-01-18',
        'open': 157.5,
        'high': 157.88,
        'low': 155.98,
        'close': 156.82,
        'volume': 3.3751E7,
        'unadjustedVolume': 3.3751E7,
        'change': 0.68,
        'changePercent': 0.432,
        'vwap': 156.89333,
        'label': 'January 18, 19',
        'changeOverTime': 0.00432
    }, {
        'date': '2019-01-22',
        'open': 156.41,
        'high': 156.73,
        'low': 152.62,
        'close': 153.3,
        'volume': 3.0394E7,
        'unadjustedVolume': 3.0394E7,
        'change': 3.11,
        'changePercent': 1.988,
        'vwap': 154.21667,
        'label': 'January 22, 19',
        'changeOverTime': 0.01988
    }, {
        'date': '2019-01-23',
        'open': 154.15,
        'high': 155.14,
        'low': 151.7,
        'close': 153.92,
        'volume': 2.31306E7,
        'unadjustedVolume': 2.31306E7,
        'change': 0.23,
        'changePercent': 0.149,
        'vwap': 153.58667,
        'label': 'January 23, 19',
        'changeOverTime': 0.00149
    }, {
        'date': '2019-01-24',
        'open': 154.11,
        'high': 154.48,
        'low': 151.74,
        'close': 152.7,
        'volume': 2.54415E7,
        'unadjustedVolume': 2.54415E7,
        'change': 1.41,
        'changePercent': 0.915,
        'vwap': 152.97333,
        'label': 'January 24, 19',
        'changeOverTime': 0.00915
    }, {
        'date': '2019-01-25',
        'open': 155.48,
        'high': 158.13,
        'low': 154.32,
        'close': 157.76,
        'volume': 3.35355E7,
        'unadjustedVolume': 3.35355E7,
        'change': -2.28,
        'changePercent': -1.466,
        'vwap': 156.73667,
        'label': 'January 25, 19',
        'changeOverTime': -0.01466
    }, {
        'date': '2019-01-28',
        'open': 155.79,
        'high': 156.33,
        'low': 153.66,
        'close': 156.3,
        'volume': 2.61921E7,
        'unadjustedVolume': 2.61921E7,
        'change': -0.51,
        'changePercent': -0.327,
        'vwap': 155.43,
        'label': 'January 28, 19',
        'changeOverTime': -0.00327
    }, {
        'date': '2019-01-29',
        'open': 156.25,
        'high': 158.13,
        'low': 154.11,
        'close': 154.68,
        'volume': 4.15872E7,
        'unadjustedVolume': 4.15872E7,
        'change': 1.57,
        'changePercent': 1.005,
        'vwap': 155.64,
        'label': 'January 29, 19',
        'changeOverTime': 0.01005
    }, {
        'date': '2019-01-30',
        'open': 163.25,
        'high': 166.15,
        'low': 160.23,
        'close': 165.25,
        'volume': 6.11098E7,
        'unadjustedVolume': 6.11098E7,
        'change': -2.0,
        'changePercent': -1.225,
        'vwap': 163.87667,
        'label': 'January 30, 19',
        'changeOverTime': -0.01225
    }, {
        'date': '2019-01-31',
        'open': 166.11,
        'high': 169.0,
        'low': 164.56,
        'close': 166.44,
        'volume': 4.07396E7,
        'unadjustedVolume': 4.07396E7,
        'change': -0.33,
        'changePercent': -0.199,
        'vwap': 166.66667,
        'label': 'January 31, 19',
        'changeOverTime': -0.00199
    }, {
        'date': '2019-02-01',
        'open': 166.96,
        'high': 168.98,
        'low': 165.93,
        'close': 166.52,
        'volume': 3.26681E7,
        'unadjustedVolume': 3.26681E7,
        'change': 0.44,
        'changePercent': 0.264,
        'vwap': 167.14333,
        'label': 'February 01, 19',
        'changeOverTime': 0.00264
    }, {
        'date': '2019-02-04',
        'open': 167.41,
        'high': 171.66,
        'low': 167.28,
        'close': 171.25,
        'volume': 3.14955E7,
        'unadjustedVolume': 3.14955E7,
        'change': -3.84,
        'changePercent': -2.294,
        'vwap': 170.06333,
        'label': 'February 04, 19',
        'changeOverTime': -0.02294
    }, {
        'date': '2019-02-05',
        'open': 172.86,
        'high': 175.08,
        'low': 172.35,
        'close': 174.18,
        'volume': 3.61016E7,
        'unadjustedVolume': 3.61016E7,
        'change': -1.32,
        'changePercent': -0.764,
        'vwap': 173.87,
        'label': 'February 05, 19',
        'changeOverTime': -0.00764
    }, {
        'date': '2019-02-06',
        'open': 174.65,
        'high': 175.57,
        'low': 172.85,
        'close': 174.24,
        'volume': 2.82396E7,
        'unadjustedVolume': 2.82396E7,
        'change': 0.41,
        'changePercent': 0.235,
        'vwap': 174.22,
        'label': 'February 06, 19',
        'changeOverTime': 0.00235
    }, {
        'date': '2019-02-07',
        'open': 172.4,
        'high': 173.94,
        'low': 170.34,
        'close': 170.94,
        'volume': 3.17417E7,
        'unadjustedVolume': 3.17417E7,
        'change': 1.46,
        'changePercent': 0.847,
        'vwap': 171.74,
        'label': 'February 07, 19',
        'changeOverTime': 0.00847
    }, {
        'date': '2019-02-08',
        'open': 168.99,
        'high': 170.66,
        'low': 168.42,
        'close': 170.41,
        'volume': 2.382E7,
        'unadjustedVolume': 2.382E7,
        'change': -1.42,
        'changePercent': -0.84,
        'vwap': 169.83,
        'label': 'February 08, 19',
        'changeOverTime': -0.0084
    }, {
        'date': '2019-02-11',
        'open': 171.05,
        'high': 171.21,
        'low': 169.25,
        'close': 169.43,
        'volume': 2.09934E7,
        'unadjustedVolume': 2.09934E7,
        'change': 1.62,
        'changePercent': 0.947,
        'vwap': 169.96333,
        'label': 'February 11, 19',
        'changeOverTime': 0.00947
    }, {
        'date': '2019-02-12',
        'open': 170.1,
        'high': 171.0,
        'low': 169.7,
        'close': 170.89,
        'volume': 2.22835E7,
        'unadjustedVolume': 2.22835E7,
        'change': -0.79,
        'changePercent': -0.464,
        'vwap': 170.53,
        'label': 'February 12, 19',
        'changeOverTime': -0.00464
    }, {
        'date': '2019-02-13',
        'open': 171.39,
        'high': 172.48,
        'low': 169.92,
        'close': 170.18,
        'volume': 2.24902E7,
        'unadjustedVolume': 2.24902E7,
        'change': 1.21,
        'changePercent': 0.706,
        'vwap': 170.86,
        'label': 'February 13, 19',
        'changeOverTime': 0.00706
    }, {
        'date': '2019-02-14',
        'open': 169.71,
        'high': 171.26,
        'low': 169.38,
        'close': 170.8,
        'volume': 2.18357E7,
        'unadjustedVolume': 2.18357E7,
        'change': -1.09,
        'changePercent': -0.642,
        'vwap': 170.48,
        'label': 'February 14, 19',
        'changeOverTime': -0.00642
    }, {
        'date': '2019-02-15',
        'open': 171.25,
        'high': 171.7,
        'low': 169.75,
        'close': 170.42,
        'volume': 2.46268E7,
        'unadjustedVolume': 2.46268E7,
        'change': 0.83,
        'changePercent': 0.485,
        'vwap': 170.62333,
        'label': 'February 15, 19',
        'changeOverTime': 0.00485
    }, {
        'date': '2019-02-19',
        'open': 169.71,
        'high': 171.44,
        'low': 169.49,
        'close': 170.93,
        'volume': 1.89728E7,
        'unadjustedVolume': 1.89728E7,
        'change': -1.22,
        'changePercent': -0.719,
        'vwap': 170.62,
        'label': 'February 19, 19',
        'changeOverTime': -0.00719
    }, {
        'date': '2019-02-20',
        'open': 171.19,
        'high': 173.32,
        'low': 170.99,
        'close': 172.03,
        'volume': 2.61144E7,
        'unadjustedVolume': 2.61144E7,
        'change': -0.84,
        'changePercent': -0.491,
        'vwap': 172.11333,
        'label': 'February 20, 19',
        'changeOverTime': -0.00491
    }, {
        'date': '2019-02-21',
        'open': 171.8,
        'high': 172.37,
        'low': 170.3,
        'close': 171.06,
        'volume': 1.72497E7,
        'unadjustedVolume': 1.72497E7,
        'change': 0.74,
        'changePercent': 0.431,
        'vwap': 171.24333,
        'label': 'February 21, 19',
        'changeOverTime': 0.00431
    }, {
        'date': '2019-02-22',
        'open': 171.58,
        'high': 173.0,
        'low': 171.38,
        'close': 172.97,
        'volume': 1.89132E7,
        'unadjustedVolume': 1.89132E7,
        'change': -1.39,
        'changePercent': -0.81,
        'vwap': 172.45,
        'label': 'February 22, 19',
        'changeOverTime': -0.0081
    }, {
        'date': '2019-02-25',
        'open': 174.16,
        'high': 175.87,
        'low': 173.95,
        'close': 174.23,
        'volume': 2.18734E7,
        'unadjustedVolume': 2.18734E7,
        'change': -0.07,
        'changePercent': -0.04,
        'vwap': 174.68333,
        'label': 'February 25, 19',
        'changeOverTime': -4.0E-4
    }, {
        'date': '2019-02-26',
        'open': 173.71,
        'high': 175.3,
        'low': 173.17,
        'close': 174.33,
        'volume': 1.70702E7,
        'unadjustedVolume': 1.70702E7,
        'change': -0.62,
        'changePercent': -0.357,
        'vwap': 174.26667,
        'label': 'February 26, 19',
        'changeOverTime': -0.00357
    }, {
        'date': '2019-02-27',
        'open': 173.21,
        'high': 175.0,
        'low': 172.73,
        'close': 174.87,
        'volume': 2.78354E7,
        'unadjustedVolume': 2.78354E7,
        'change': -1.66,
        'changePercent': -0.958,
        'vwap': 174.2,
        'label': 'February 27, 19',
        'changeOverTime': -0.00958
    }, {
        'date': '2019-02-28',
        'open': 174.32,
        'high': 174.91,
        'low': 172.92,
        'close': 173.15,
        'volume': 2.82154E7,
        'unadjustedVolume': 2.82154E7,
        'change': 1.17,
        'changePercent': 0.671,
        'vwap': 173.66,
        'label': 'February 28, 19',
        'changeOverTime': 0.00671
    }, {
        'date': '2019-03-01',
        'open': 174.28,
        'high': 175.15,
        'low': 172.89,
        'close': 174.97,
        'volume': 2.58862E7,
        'unadjustedVolume': 2.58862E7,
        'change': -0.69,
        'changePercent': -0.396,
        'vwap': 174.33667,
        'label': 'March 01, 19',
        'changeOverTime': -0.00396
    }, {
        'date': '2019-03-04',
        'open': 175.69,
        'high': 177.75,
        'low': 173.97,
        'close': 175.85,
        'volume': 2.74362E7,
        'unadjustedVolume': 2.74362E7,
        'change': -0.16,
        'changePercent': -0.091,
        'vwap': 175.85667,
        'label': 'March 04, 19',
        'changeOverTime': -9.1E-4
    }, {
        'date': '2019-03-05',
        'open': 175.94,
        'high': 176.0,
        'low': 174.54,
        'close': 175.53,
        'volume': 1.97374E7,
        'unadjustedVolume': 1.97374E7,
        'change': 0.41,
        'changePercent': 0.233,
        'vwap': 175.35667,
        'label': 'March 05, 19',
        'changeOverTime': 0.00233
    }, {
        'date': '2019-03-06',
        'open': 174.67,
        'high': 175.49,
        'low': 173.94,
        'close': 174.52,
        'volume': 2.08104E7,
        'unadjustedVolume': 2.08104E7,
        'change': 0.15,
        'changePercent': 0.086,
        'vwap': 174.65,
        'label': 'March 06, 19',
        'changeOverTime': 8.6E-4
    }, {
        'date': '2019-03-07',
        'open': 173.87,
        'high': 174.44,
        'low': 172.02,
        'close': 172.5,
        'volume': 2.47964E7,
        'unadjustedVolume': 2.47964E7,
        'change': 1.37,
        'changePercent': 0.788,
        'vwap': 172.98667,
        'label': 'March 07, 19',
        'changeOverTime': 0.00788
    }, {
        'date': '2019-03-08',
        'open': 170.32,
        'high': 173.07,
        'low': 169.5,
        'close': 172.91,
        'volume': 2.39994E7,
        'unadjustedVolume': 2.39994E7,
        'change': -2.59,
        'changePercent': -1.521,
        'vwap': 171.82667,
        'label': 'March 08, 19',
        'changeOverTime': -0.01521
    }, {
        'date': '2019-03-11',
        'open': 175.49,
        'high': 179.12,
        'low': 175.35,
        'close': 178.9,
        'volume': 3.2011E7,
        'unadjustedVolume': 3.2011E7,
        'change': -3.41,
        'changePercent': -1.943,
        'vwap': 177.79,
        'label': 'March 11, 19',
        'changeOverTime': -0.01943
    }, {
        'date': '2019-03-12',
        'open': 180.0,
        'high': 182.67,
        'low': 179.37,
        'close': 180.91,
        'volume': 3.24676E7,
        'unadjustedVolume': 3.24676E7,
        'change': -0.91,
        'changePercent': -0.506,
        'vwap': 180.98333,
        'label': 'March 12, 19',
        'changeOverTime': -0.00506
    }];
    public pieChartOptions: ChartOptions = {
        responsive: true,
        title: {
            display: true,
            text: 'Ventas por Sucursal'
        },
    };
    public pieChartLegend = true;


    public pieChartLabels = ['Sucursal1', 'Sucursal2', 'Sucursal3'];
    public pieChartData = [150, 180, 80];


    constructor(
        private router: Router,
        private http: HttpClient
    ) {
    }

    ngOnInit(): void {
        this.getDataLine();
    }

    logout() {
        this.router.navigate(['/login']);
    }

    getDataLine() {

        const history = this.history;

        this.chartLabelsLine = [];
        this.chartDataLine[0].data = [];

        for (const entry of history) {
            this.chartLabelsLine.push(entry.date);
            this.chartDataLine[0].data.push(entry['close']);
        }
    }

}
