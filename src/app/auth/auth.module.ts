import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {LoginPage} from './login/login.page';
import {CompanySelectionPage} from './company-selection/company-selection.page';
import {BranchSelectionPage} from './branch-selection/branch-selection.page';
import {RoleSelectionPage} from './role-selection/role-selection.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: LoginPage
            },
            {
                path: 'company-selection',
                component: CompanySelectionPage
            },
            {
                path: 'branch-selection',
                component: BranchSelectionPage
            },
            {
                path: 'role-selection',
                component: RoleSelectionPage
            }
        ])
    ],
    declarations: [
        LoginPage,
        CompanySelectionPage,
        BranchSelectionPage,
        RoleSelectionPage
    ]
})
export class AuthModule {
}
