import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SifApiService} from '../../services/sif-api.service';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-branch-selection',
    templateUrl: 'branch-selection.page.html',
})
export class BranchSelectionPage implements OnInit {

    public branches = [];
    public infoCompany;

    constructor(
        private router: Router,
        private apiService: SifApiService,
    ) {
        this.infoCompany = JSON.parse(localStorage.getItem('infoCompany'));
    }

    ngOnInit(): void {
        this.apiService.meBranch(this.infoCompany.id).subscribe(data => {
            this.branches = data.data;
        });
    }

    branchSelected(branch) {
        console.log(branch);
        localStorage.setItem('infoBranch', JSON.stringify({id: branch.id, name: branch.name}));
        this.router.navigate(['/login/role-selection']);
    }

    return() {
        this.router.navigate(['/login/company-selection']);
    }

    logout() {
        localStorage.clear();
        this.router.navigate(['/login']);
    }
}
