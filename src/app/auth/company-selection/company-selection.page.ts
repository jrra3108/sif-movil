import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SifApiService} from '../../services/sif-api.service';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-company-selection',
    templateUrl: 'company-selection.page.html',
})
export class CompanySelectionPage implements OnInit {

    public companies = [];

    constructor(
        private router: Router,
        private apiService: SifApiService,
        private toastr: ToastrService
    ) {}

    ngOnInit(): void {
        this.apiService.meCompany().subscribe( dataComp => {
            console.log(dataComp);
            this.companies = dataComp.data;
            if (this.companies.length === 0) {
                this.toastr.warning('Su usuario no posee roles asociados, favor contacte al administrador del sistema.');
                this.logout();
            }
        });
    }

    companySelected(company) {
        console.log(company);
        localStorage.setItem('infoCompany', JSON.stringify({id: company.id, name: company.name}));
        this.router.navigate(['/login/branch-selection']);
    }

    logout() {
        localStorage.clear();
        this.router.navigate(['/login']);
    }
}
