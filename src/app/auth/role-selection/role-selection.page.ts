import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SifApiService} from '../../services/sif-api.service';

@Component({
    selector: 'app-role',
    templateUrl: 'role-selection.page.html',
})
export class RoleSelectionPage implements OnInit {

    public roles = [];
    public infoBranch;

    moduleList = [];
    public menuApp = [];
    t = [];
    menu = [];
    roleId: string;

    constructor(
        private router: Router,
        private apiService: SifApiService,
    ) {
        this.infoBranch = JSON.parse(localStorage.getItem('infoBranch'));
    }

    ngOnInit(): void {
        this.apiService.meRole(this.infoBranch.id).subscribe(data => {
            this.roles = data.data;
        });
    }

    roleSelected(role) {
        this.roleId = role.id;
        localStorage.setItem('infoRole', JSON.stringify({id: role.id, name: role.name}));
        this.getModules();
    }

    return() {
        this.router.navigate(['/login/branch-selection']);
    }

    logout() {
        localStorage.clear();
        this.router.navigate(['/login/login']);
    }

    getModules() {
        this.moduleList = [];
        this.apiService.getModules().subscribe(response => {
            this.moduleList.push({
                id: 'SIF',
                title: 'Sistema de informacion',
                type: 'group',
                children: [
                    {
                        id: 'dashboard',
                        title: 'Dashboard',
                        type: 'item',
                        icon: 'home',
                        url: '/dashboard',
                    }
                ]
            });
            response.catalog.forEach(  mod => {
                this.moduleList.push({
                    id: mod.id,
                    title: mod.name || undefined,
                    icon: 'settings_applications',
                    type: 'collapsable',
                    children: []
                });
            });
            this.getMenu();
        });
    }

    getMenu() {
        this.apiService.getMenuAppByRole(this.roleId).subscribe(response => {

            this.menu = [{
                title: 'Dashboard',
                url: '/home',
                icon: 'home',
            }];
            this.menuApp = [
            ];

            response.data.forEach( opMenu => {
                for (let i = 0; i < opMenu.menus.length; i++) {
                    this.menu.push(
                        {
                            id: opMenu.menus[i].id,
                            title: opMenu.menus[i].name,
                            icon: opMenu.menus[i].icon,
                            url: opMenu.menus[i].url,
                            moduleId: opMenu.menus[i].module.id,
                            parentId: opMenu.menus[i].principal,
                        }
                    );
                }
            });
            // this.list_to_tree(this.menu).forEach( item => {
            //     this.moduleList.forEach( modItem => {
            //         if (item.moduleId === modItem.id) {
            //             modItem.children.push(item);
            //         }
            //     });
            // });
            this.menuApp = this.menu;
            localStorage.setItem('menu', JSON.stringify(this.menuApp));
            // window.location.reload();
            this.router.navigate(['/home']);
            window.location.href = '/home';
        });
    }

    list_to_tree(list) {
        const map = {};
        let node;
        const roots = [];
        let i;
        for ( i = 0; i < list.length; i += 1) {
            map[list[i].id] = i;
            list[i].children = [];
        }
        for (i = 0; i < list.length; i += 1) {
            node = list[i];
            if (node.parentId !== '0') {
                list[map[node.parentId]].type = 'collapsable';
                list[map[node.parentId]].children.push(node);
            } else {
                roots.push(node);
            }
        }
        return roots;
    }
}
