import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {SifApiService} from '../../services/sif-api.service';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-login',
    templateUrl: 'login.page.html',
})
export class LoginPage {
    username: '';
    password: '';
    constructor(
        private router: Router,
        private apiService: SifApiService,
        private toastr: ToastrService
    ) {}

    login() {
        if (this.username !== '' && this.password) {
            const user = {
                username: this.username,
                password: this.password,
            };
            this.apiService.login(user).subscribe(resp => {
                if (resp.header.code === 200) {
                    localStorage.setItem('token', resp.data.access_token);
                    this.apiService.me().subscribe( data => {
                        localStorage.setItem('infoUser', JSON.stringify(data.data));
                        this.toastr.success('Inicio de sesión con exito');
                        this.router.navigate(['/login/company-selection']);
                    });
                } else {
                    this.toastr.warning('Usuario/Contraseña incorrectos');
                }
            });
        } else {
            this.toastr.warning('Favor complete todos los campos');
        }
    }
}
