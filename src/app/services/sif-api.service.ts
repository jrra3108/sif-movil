import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class SifApiService {
    private url = environment.apiUrl;

    constructor(private httpClient: HttpClient) {}

    public login(user): Observable<any> {
        return this.httpClient.post<any>(`${this.url}auth/login`, user);
    }

    public me(): Observable<any> {
        return this.httpClient.get<any>(`${this.url}auth/login/me`);
    }

    public meCompany(): Observable<any> {
        return this.httpClient.get<any>(`${this.url}auth/login/company`);
    }

    public meBranch(companyId: string): Observable<any> {
        return this.httpClient.get<any>(`${this.url}auth/login/branch/${companyId}`);
    }

    public meRole(branchId: string): Observable<any> {
        return this.httpClient.get<any>(`${this.url}auth/login/role/${branchId}`);
    }

    getModules(): Observable<any> {
        return this.httpClient.get<any>(`${this.url}admin/module/catalog`);
    }

    getMenuAppByRole(roleId: string): Observable<any> {
        return this.httpClient.get<any>(`${this.url}admin/menu/options/${roleId}`);
    }
}
