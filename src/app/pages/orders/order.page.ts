import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-order',
    templateUrl: 'order.page.html',
})
export class OrderPage implements OnInit {

    constructor(
        private router: Router
    ) {
    }

    ngOnInit() {
    }

    logout() {
        this.router.navigate(['/login']);
    }
}
